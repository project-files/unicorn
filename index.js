var path = require('path');
var argv = require('minimist')( process.argv.slice(2) );
var url = 'file://'+ path.resolve('index.html');
if(argv.url) { url = argv.url; }
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
var mainWindow = null;
app.on('window-all-closed', function() { if (process.platform != 'darwin') { app.quit(); } });
app.on('ready', function() {
  mainWindow = new BrowserWindow({"auto-hide-menu-bar": true, width: 1024, height: 600});
  if (argv.max) mainWindow.maximize();
  if (argv.dev) mainWindow.webContents.openDevTools();
  mainWindow.loadURL(url);
  mainWindow.on('closed', function() {
    mainWindow = null;
  });
});
