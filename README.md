Unicorn: The Editable Application User Interface Hosting and Debugging Environment
---

### Unicorn <3 Dragonfly ###
![Screenshot](screenshot1.png)

### A Dragonfly in Fullscreen ###
![Screenshot](screenshot2.png)

### Unicorn Installation ###

    git clone https://gitlab.com/project-files/unicorn.git
    cd unicorn
    npm i
    [sudo] npm link dragonfly
    npm start

Please note: the example above references a private build of dragonfly
purchased from http://dragonfly.hominusnocturna.com/ If you would like
to get a quick feel for the demo build of dragonfly you can substitute
***npm start*** above with the following line:

    npm start -- --url http://dragonfly.hominusnocturna.com

Any compatible Editable Application User Interface Environment will work
with Unicorn. The API requires a pre-build jQuery like library that
can reference web resources in node_modules.

***Unicorn is an Open Source Project*** released under ***MIT License***
The same license chosen by jQuery, .NET Core, and Rails.

### Becoming one with Unicorn ###

For simplicity the following assumes you will be linking dragonfly.

You must link your editable application before executing (npm link dragonfly)
in the unicorn directory. You will find link documentation [here](https://docs.npmjs.com/cli/link) but the short version is enter your
dragonfly directory (cd your-dragonfly-package) and run the following
command in order to link dragonfly to you global (OS level) module library:

    [sudo] npm link

From there you can link the now global dragonfly, down to your unicorn
clone with ***[sudo] npm link dragonfly*** as shown in main installation
section above.

Dragonfly will now exist in unicorn/node_modules folder  where it will
be referenced by the unicorn/index.html.

### Unicorn License ###

The MIT License (MIT)

Copyright (c) 2016 Hominus Nocturna www.hominusnocturna.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
